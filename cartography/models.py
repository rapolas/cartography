
COUNTRIES = (
    'Afghanistan', 'Åland Islands', 'Albania', 'Algeria',
    'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica',
    'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba',
    'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain',
    'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin',
    'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia-Herzegovina', 'Botswana',
    'Bouvet Island', 'Brazil', 'British Indian Ocean Territory',
    'British Virgin Islands', 'Brunei', 'Bulgaria', 'Burkina Faso',
    'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde',
    'Cayman Islands', 'Central African Republic', 'Chad', 'Chile',
    'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia',
    'Comoros', 'Congo', 'Cook Islands', 'Costa Rica',
    'Côte d\'Ivoire', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic',
    'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic',
    'DR Congo', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea',
    'Eritrea', 'Estonia', 'Ethiopia', 'Faeroe Islands',
    'Falkland Islands', 'Fiji', 'Finland', 'France', 'French Guiana',
    'French Polynesia', 'French Southern Territories', 'Gabon',
    'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece',
    'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala',
    'Guernsey', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti',
    'Heard Island and McDonald Islands', 'Honduras',
    'Hong Kong (China)', 'Hungary', 'Iceland', 'India', 'Indonesia',
    'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan',
    'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Kosovo', 'Kuwait',
    'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia',
    'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg',
    'Macao (China)', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia',
    'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique',
    'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia',
    'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat',
    'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal',
    'Netherlands', 'Netherlands Antilles', 'New Caledonia',
    'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue',
    'Norfolk Island', 'North Korea', 'Northern Mariana Islands',
    'Norway', 'Oman', 'Pakistan', 'Palau',
    'Palestinian Occupied Territory', 'Panama', 'Papua New Guinea',
    'Paraguay', 'Peru', 'Philippines', 'Pitcairn', 'Poland',
    'Portugal', 'Puerto Rico', 'Qatar', 'Reunion', 'Romania', 'Russia',
    'Rwanda', 'Saint Helena', 'Saint Kitts and Nevis', 'Saint Lucia',
    'Saint Pierre and Miquelon', 'Saint Vincent and The Grenadines',
    'Samoa', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia',
    'Senegal', 'Serbia', 'Serbia and Montenegro', 'Seychelles',
    'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia',
    'Solomon Islands', 'Somalia', 'South Africa',
    'South Georgia and the South Sandwich Islands', 'South Korea',
    'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname',
    'Svalbard and Jan Mayen', 'Swaziland', 'Sweden', 'Switzerland',
    'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand',
    'The Vatican', 'Timor-Leste', 'Togo', 'Tokelau', 'Tonga',
    'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan',
    'Turks and Caicos', 'Tuvalu', 'Uganda', 'Ukraine',
    'United Arab Emirates', 'United Kingdom',
    'United States Minor Outlying Islands', 'Uruguay', 'USA',
    'Uzbekistan', 'Vanuatu', 'Venezuela', 'Viet Nam',
    'Virgin Islands (US)', 'Wallis and Futuna', 'Western Sahara',
    'Yemen', 'Zambia', 'Zimbabwe'
)


class Tweet:
    def __init__(self, tweet_id, text):
        self.tweet_id = tweet_id
        self._text = text

    @property
    def text(self):
        """Remove new line characters from the tweet."""
        return self._text.replace('\n', ' ')

    @property
    def countries(self):
        """Return a set of countries parsed from the tweet."""
        data = set()

        for country in COUNTRIES:
            if country in self.text:
                data.add(country)
        return data

    @staticmethod
    def from_dict(data):
        """Construct Tweet object from given dictionary data."""
        tweet_id = data.get('id', '')
        text = data.get('text', '')
        return Tweet(tweet_id, text)
