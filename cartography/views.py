from django.conf import settings
from django.shortcuts import render

import twitter

from . models import Tweet


def fetch_tweets():
    """Fetch tweets from api."""
    api = twitter.Api(
        consumer_key=settings.CONSUMER_KEY,
        consumer_secret=settings.CONSUMER_SECRET,
        access_token_key=settings.ACCESS_TOKEN_KEY,
        access_token_secret=settings.ACCESS_TOKEN_SECRET,
    )
    statuses = api.GetUserTimeline(
        screen_name=settings.TWITTER_NAME,
        count=settings.TWEET_COUNT
    )

    tweets = []
    for status in statuses:
        tweets.append(Tweet.from_dict(status.AsDict()))

    return tweets


def index(request):
    """Main landing page."""
    tweets = fetch_tweets()
    countries = [country for tweet in tweets for country in tweet.countries]
    context = {"tweets": tweets, "countries": countries}
    return render(request, 'cartography/index.html', context)
