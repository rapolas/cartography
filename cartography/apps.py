from django.apps import AppConfig


class CartographyConfig(AppConfig):
    name = 'cartography'
