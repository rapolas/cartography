# README #

Simple django webapp to display latest tweets from MaplecroftRist
twitter feed.

### How do I get set up? ###

* Checkout repository: `git clone https://bitbucket.org/rapolas/cartography.git`
* Create virtualenv and activate it: `python3 -m venv venv && source venv/bin/activate`
* Install requirements: `pip install requirements.txt`
* Edit mysite/settings.py file and put appropriate keys and tokens
  at the very bottom. To get those, follow instructions [here](https://python-twitter.readthedocs.io/en/latest/getting_started.html)
* Run webapp: `python manage.py runserver`
